FROM openjdk:8-alpine
LABEL maintainer="Sergio Rivas Medina -> sergiorm96@gmail.com"

ADD ./target/ /app
EXPOSE 9007
WORKDIR /app
ENTRYPOINT ["java", "-jar", "bff-dispatcher-0.0.1-SNAPSHOT.jar"]